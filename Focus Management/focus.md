## Question 1

# What is Deep Work?

Deep work is something that you can not just do, but also enjoy. In this talk, Cal Newport and Lex Fridman discuss deep work, the benefits of deep work for achieving true mastery, and how to stay motivated after finishing your first deep work project.

## Question 2

# Paraphrase all the ideas in the above videos and this one in detail.

Description in Points:-

- Optimal duration for deep work is at least 1 hour. Not to switch between a context for at least an hour during deep work.
- Deadlines/time-block schedules are a kind of motivational signal, it helps one by avoiding procrastination things and avoiding taking unnecessary breaks in the middle.
- Deadlines should be reasonable to a humane capacity, too much pressure can reduce productivity.
- Breaks and distractions must be scheduled.
- Deep work must be made a habit.
- Get enough sleep.

## Question 3

# How can you implement the principles in your day-to-day life?

Some of the below Points:-

- Keep distractions aside.
- Don't look at smartphones or social media unnecessarily which wastes a lot of time, and reduces concentration.
- Make deep work a habit, and practice it every day.
- Having adequate sleep.

## Question 4

# Your key takeaways from the video.

Some key Points:-

- Social media offers shiny treats in exchange for minutes of user's attention and bytes of personal data.
- Many social media companies hire attention engineers, who gather principles from gambling, and other such, and make these products as addictive as possible
- Saying Social media usage is not harmless is not enough, one needs to realize the potential harm social media brings with its usage.
- Social media brings with it multiple, well-documented, and significant harms. Such as.

  1. Losing the Ability to sustain concentration & Harm to professional success: as social media are designed to be addictive, so much of the attention is lost to this. This permanently reduces the capacity to concentrate and do more necessary in a competitive economy.
  2. Psychological loss: More the usage of social media, the more likely to feel lonely or isolated, which can make one feel inadequate and can increase rates of depression.
  3. A report says, along with the rise of the ubiquitous smartphone and social media on the college campus, there is an explosion of anxiety-related disorders.

- It may feel discomfort after leaving social media, for around two weeks, but it's a true detox process.
