# Grit and Growth Mindset

## Question 1

## Paraphrase (summarize) the video in a few lines. Use your own words.

The video talks about the importance of grit and a growth mindset. It's about the significance of grit over IQ and other parameters. Grittier people are more likely to become successful than a talented ones. The only way to develop grit is through a growth mindset.

## Question 2

## What are your key takeaways from the video to take action on?

- For one to succeed in life, it's not just IQ but grit that matters most.

- Grit is the hunger for a goal.

- Don't fall during failure. Instead, learn and improve.

- To persevere during failure.

- Having a growth mindset improves one's performance.

## Question 3

## Paraphrase (summarize) the video in a few lines in your own words.

The video talks about the two kinds of mindsets

- Growth Mindset - How a growth mindset can help in the progress and improve one's performance.

- Fixed Mindset - How a fixed mind leads to stagnation.

Many companies have implemented this growth mindset in their culture.

## Question 4

## What are your key takeaways from the video to take action on?

- We should look at efforts as a useful thing.

- We should embrace challenges and work through them.

- We should see mistakes as learning opportunities

- We should appreciate feedback and use it to improve ourselves.

- Not getting discouraged during failure.

## Question 5

## What is the Internal Locus of Control? What is the key point in the video?

- Locus of Control is the degree one belief that one has control over their life. And Internal Locus of Control is the belief that How much work one put into something that one has complete control over.

- Key Point in the video: Internal Local Control is the key to motivation.

- To be motivated all the time, one can best achieve it by simply solving the issues in one's own life, taking some time, and appreciating that one action solved the problems.

## Question 6

## Paraphrase (summarize) the video in a few lines in your own words.

- Belief that I can figure things out and I can get better enables us lifelong improvement.

- Question your assumptions as to who am I?, Am I capable of that?

- Develop your life curriculum.

- During failure time honor the struggle.

## Question 7

What are your key takeaways from the video to take action on?

- Self-belief and self-confidence are the base for growth.
- Have a life curriculum, and work towards that.
- Don't get frustrated during the process, and honor the struggle. The difficulty will build a stronger character and help subsequently, so keep going.
- To keep persistence when getting knocked out.

## Question 8

## What are one or more points that you want to take action on from the manual?

- I will follow the steps required to solve problems:
  Relax
  Focus - What is the problem? What do I need to know to solve it?
  Understand - Documentation, Google, Stack Overflow, GitHub Issues, Internet
  Code
  Repeat
- I will understand the users very well. I will serve them and society by writing rock solid excellent software
