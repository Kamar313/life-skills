# Tiny Habits

## Question 1

## Your takeaways from the video?

- A real change comes through hundreds of small decisions of small habits.
- One can make drastic changes with minor tweaks to the daily routine. It does not require something big.
- To develop a habit, have motivation, ability, and a trigger.
- Plant a tiny seed in the right spot and it will grow without coaxing.

## Question 2

## Your takeaways from the video?

- Tiny Habits Methods involve 3 parts.
  1.  Shrink: Hard tasks require more motivation to break down the task into the tiniest version so it will not require a lot of motivation.
  2.  Identify an Action Prompt: Set the habit you want to make right after something you already do.
  3.  Shine: Celebrate tiny successes as you celebrate big achievements.

## Question 3

## How can you use B = MAP to make making new habits easier?

- Reduce the quantity(Break into smaller chunks)/ Just do the first step.
- Identify an action prompt, and attach it to the habit.
- Celebrate the small victories.

## Question 4

## Why it is important to "Shine" or Celebrate after each successful completion of a habit?

Celebrating small victories and rewarding ourselves attracts us towards the habit of continuing for the long term and thus helps us become a better version of ourselves.

## Question 5

## Your takeaways from the video (Minimum 5 points)

- Habits develop and get internalise over time.
- Create barriers to your bad habits.
- Make good habits easier to follow.
- Make a proper plan and have clarity for achieving any behavior.
- Environment affect one's behavior, so look for a conducive environment.
- Plan for Optimizing the beginning, not the ending.
- Follow the Two-minute rule: If it takes two minutes or less, does it instantly, don't procrastinate it.
- Be consistent, and don't break the chain of small habits.

## Question 6

## Write about the book's perspective on habit formation from the lens of Identity, processes, and outcomes.

- To solve the problem in the long term we should change our identity.

- Make habits part of your identity. When solving a problem based on outcomes we only solve them temporarily.

- The most effective way to change your habits is to focus not on what you want to achieve, but on who you wish to become.

- Most of us work from outcome to identity rather than identity to the outcome.

## Question 7

## Write about the book's perspective on how to make a good habit easier.

1. Make it invisible.
2. Make it unattractive.
3. Make it difficult.
4. Make it unsatisfying.

## Question 9

## Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

I will read a blog every day related to software engineering

- I will subscribe to some standard channels and blogs
- I will work on the new things learned and maintain a GitHub profile and maintain it.

## Question 10

## Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

I will stop looking for easier methods, and rather choose standard methods of software engineering.
