# Good Practices for Software Development.

## Questions 1

## What is your one major takeaway from each one of the 6 sections? So 6 points in total?

- Sometimes despite all efforts, the requirements are still vague. So during the implementation phase, get frequent feedback so that you are on track and everyone is on the same page.
- Do not miss calls. If you cannot talk immediately, receive the call and inform them you will call in back in 5-10 min, and call them back. This is a much better alternative than letting it go down as a missed call.
- Explain the problem clearly, and mention the solutions you tried out to fix the problem.
- Join the meetings 5-10 mins early to get some time with your team members.
- Remember they have their work to do as well. Pick and choose your communication medium depending on the situation.
- Always keep your phone on silent. Remove notifications from the home screen. Only keep notifications for work-related apps.

## Which area do you think you need to improve on? What are your ideas to make progress in that area?

I think I need to improve my coding skills. I am developing a website and I know that I have many things that need to be fixed but if I was going to make progress, I would not just focus on this but also make sure that everything works properly and tests are run as well. I will try to spend more in writing code so I will improve myself in that.
