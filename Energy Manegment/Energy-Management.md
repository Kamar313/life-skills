## Question 1:

# What are the activities you do that make you relax - Calm quadrant?

Some Point mentioned below:-

- Watch movies in a theater.
- Go for Outing.
- Singing song for self.

## Question 2:

# When do you find getting into the Stress quadrant?

I am feeling not good in summer.

## Question 3:

# How do you understand if you are in the Excitement quadrant?

On new experiences and are most inspired by challenges, goals, and achieving success.

## Question 4:

# Paraphrase the Sleep is your Superpower video in detail.

Due to lack of sleep:-

- Lack of sleep also has a bad impact on your immune system, according to a study.
- Sleep deprivation has been linked to an increased risk of cardiovascular disease.
- ack of sleep can also affect your DNA, making your body shun immune cells and increasing the production of cancer-producing cells.

## Question 5:

# What are some ideas that you can implement to sleep better?

Try making your bed a more comfortable place for yourself. When your mattress is just right for you, it can be hard to get out of bed.

## Question 6:

# Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

- Studies have shown that exercising can increase the volume of the hippocampus.

- Memory gets better by exercising.

- Exercising reduces the risk of cognitive diseases.

- Exercising makes your pre-frontal cortex more immune to cognitive diseases.

- Exercising can increase focus span.

## Question 7:

## What are some steps you can take to exercise more?

Exercise more to increase your strength and endurance, lose weight, improve mental health and decrease the risk of heart disease, diabetes, and cancer.
